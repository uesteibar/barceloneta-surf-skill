const resolver = require('./conditions');
const axios = require('axios');
const moment = require('moment-timezone');

jest.mock('axios');

const now = moment('2018-12-25T09:00:00').unix();
Date.now = jest.fn(() => now);

const mockResponse = (min = 0.2, max = 0.3) => [
  {
    localTimestamp: now - 20000,
    swell: {
      minBreakingHeight: 0.2,
      maxBreakingHeight: 0.3,
    },
  },
  {
    localTimestamp: now - 2000,
    swell: {
      minBreakingHeight: 0.2,
      maxBreakingHeight: 0.3,
    },
  },
  {
    localTimestamp: now + 2000,
    swell: {
      minBreakingHeight: min,
      maxBreakingHeight: max,
    },
  },
  {
    localTimestamp: now - 20000,
    swell: {
      minBreakingHeight: 0.1,
      maxBreakingHeight: 0.3,
    },
  },
];

describe('when minBreakingHeight is higher than 0.3', () => {
  const minBreakingHeight = 0.4;

  describe('when minBreakingHeight is equal to maxBreakingHeight', () => {
    describe('when no date or time requested', () => {
      test('returns conditions for the given time', async () => {
        const resp = { data: mockResponse(minBreakingHeight, 0.4) };
        axios.get.mockResolvedValue(resp);

        expect((await resolver(now, {})).message).toBe(
          'Se esperan olas de 0.4 metros ahora mismo.'
        );
      });
    })

    describe('when date requested', () => {
      test('returns conditions for the given time', async () => {
        const resp = { data: mockResponse(minBreakingHeight, 0.4) };
        axios.get.mockResolvedValue(resp);

        expect((await resolver(now, {requestedDate: true})).message).toBe(
          'Se esperan olas de 0.4 metros el martes 25 de diciembre.'
        );
      });
    })

    describe('when date and time requested', () => {
      test('returns conditions for the given time', async () => {
        const resp = { data: mockResponse(minBreakingHeight, 0.4) };
        axios.get.mockResolvedValue(resp);

        expect((await resolver(now, {requestedDate: true, requestedTime: true})).message).toBe(
          'Se esperan olas de 0.4 metros el martes 25 de diciembre a las 9.'
        );
      });
    })
  });

  describe('when no date or time requested', () => {
    test('returns conditions for the given time', async () => {
      const resp = { data: mockResponse(minBreakingHeight, 0.5) };
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {})).message).toBe(
        'Se esperan olas de entre 0.4 y 0.5 metros ahora mismo.'
      );
    });
  })

  describe('when date requested', () => {
    test('returns conditions for the given time', async () => {
      const resp = { data: mockResponse(minBreakingHeight, 0.5) };
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {requestedDate: true})).message).toBe(
        'Se esperan olas de entre 0.4 y 0.5 metros el martes 25 de diciembre.'
      );
    });
  })

  describe('when date and time requested', () => {
    test('returns conditions for the given time', async () => {
      const resp = { data: mockResponse(minBreakingHeight, 0.5) };
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {requestedDate: true, requestedTime: true})).message).toBe(
        'Se esperan olas de entre 0.4 y 0.5 metros el martes 25 de diciembre a las 9.'
      );
    });
  })
});

describe('when minBreakingHeight is lower than 0.4', () => {
  const resp = { data: mockResponse(0.3, 0.5) };
  describe('when no date or time requested', () => {
    test('returns conditions for the given time', async () => {
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {})).message).toBe(
        'No se esperan buenas condiciones para el surf ahora mismo.'
      );
    });
  })

  describe('when date requested', () => {
    test('returns conditions for the given time', async () => {
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {requestedDate: true})).message).toBe(
        'No se esperan buenas condiciones para el surf el martes 25 de diciembre.'
      );
    });
  })

  describe('when date and time requested', () => {
    test('returns conditions for the given time', async () => {
      axios.get.mockResolvedValue(resp);

      expect((await resolver(now, {requestedDate: true, requestedTime: true})).message).toBe(
        'No se esperan buenas condiciones para el surf el martes 25 de diciembre a las 9.'
      );
    });
  })
});

describe('when conditions are not found', () => {
  test('returns conditions for the given time', async () => {
    const resp = { data: [] };
    axios.get.mockResolvedValue(resp);

    expect((await resolver(now, {})).error).toBe('NOT_FOUND');
  });
});
