const fetchConditions = require('../api/conditions');
const moment = require('moment-timezone');

const findTargetForecast = (forecasts, targetTime) => {
  return forecasts.find(forecast => {
    diff = targetTime - forecast.localTimestamp;
    return diff > -10800 && diff <= 0;
  });
};

const presenters = {
  bad: ({formattedTime}) => `No se esperan buenas condiciones para el surf ${formattedTime}.`,
  single: ({formattedTime, min}) => `Se esperan olas de ${min} metros ${formattedTime}.`,
  range: ({formattedTime, min, max}) => `Se esperan olas de entre ${min} y ${max} metros ${formattedTime}.`,
}

const presentTimestamp = (timestamp, { requestedTime, requestedDate }) => {
  const toFormat = moment
    .unix(timestamp)
    .locale('es')

  return {
    current: 'ahora mismo',
    complete: toFormat.format('[el] dddd D [de] MMMM [a las] h'),
    date: toFormat.format('[el] dddd D [de] MMMM'),
  }
}

const responseBuilder = ({ min, max, formattedTime }) => {
  if (min < 0.4) return presenters.bad({ formattedTime })
  if (min == max) return presenters.single({formattedTime, min})
  if (min != max) return presenters.range({formattedTime, min, max})
};

const formatTime = ({timestamp, requestedDate, requestedTime}) => {
  const timeFormats = presentTimestamp(timestamp, {requestedTime, requestedDate})

  if (requestedDate && requestedTime) return timeFormats.complete
  if (requestedDate && !requestedTime) return timeFormats.date
  if (!requestedDate && !requestedTime) return timeFormats.current
}

module.exports = async (targetTime, {requestedDate, requestedTime}) => {
  const response = await fetchConditions();
  const forecast = findTargetForecast(response.data, targetTime);
  if (forecast) {
    const { maxBreakingHeight, minBreakingHeight } = forecast.swell;

    return {
      message: responseBuilder({
        min: minBreakingHeight,
        max: maxBreakingHeight,
        formattedTime: formatTime({timestamp: targetTime, requestedDate, requestedTime}),
      }),
    };
  } else {
    return { error: 'NOT_FOUND' };
  }
};
