const AmazonDateParser = require('amazon-date-parser');
const moment = require('moment-timezone');

const now = () => moment().unix();

const buildDate = ({ time, date }) => moment(date + ' ' + time).unix();

const today = () =>
  moment()
    .tz('Europe/Madrid')
    .format('YYYY-MM-DD');

const normalizeAmazonDate = amazonDate => {
  if (!amazonDate) return null;

  const parsedDate = new AmazonDateParser(amazonDate);
  return moment(parsedDate.startDate)
    .tz('Europe/Madrid')
    .format('YYYY-MM-DD');
};

module.exports = ({ time, date }) => {
  if (!time && !date) return now();

  return buildDate({
    time: time || '09:00',
    date: normalizeAmazonDate(date) || today(),
  });
};
