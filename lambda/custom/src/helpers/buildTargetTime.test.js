const subject = require('./buildTargetTime');
const moment = require('moment-timezone');

const now = new Date()
Date.now = jest.fn(() => now);

describe('when both time and date are missing', () => {
  test('returns current date', async () => {
    expect(subject({})).toBe(moment().unix());
  });
});

describe('when only date is present', () => {
  test('constructs date for 9am', async () => {
    const args = {
      date: '2015-11-24',
    };

    const expectedDate = moment('2015-11-24 09:00').unix();
    expect(subject(args)).toBe(expectedDate);
  });
});

describe('when only time is present', () => {
  test('constructs date for current date', async () => {
    const args = {
      time: '10:00',
    };

    const date = moment()
      .tz('Europe/Madrid')
      .format('YYYY-MM-DD');
    const expectedDate = moment(date + ' 10:00').unix();
    expect(subject(args)).toBe(expectedDate);
  });
});

describe('when both date and time are present', () => {
  test('constructs date for current date', async () => {
    const args = {
      time: '10:00',
      date: '2015-11-24',
    };

    date = new Date().toISOString().slice(0, 10);
    const expectedDate = moment('2015-11-24 10:00').unix()
    expect(subject(args)).toBe(expectedDate);
  });
});
