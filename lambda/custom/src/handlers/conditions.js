const resolver = require('../resolvers/conditions');
const buildTargetTime = require('../helpers/buildTargetTime');

const handleError = handlerInput =>
  handlerInput.responseBuilder
    .speak(
      'No he podido encontrar la información. Por favor, inténtelo otra vez.'
    )
    .withShouldEndSession(true)
    .getResponse();

const handleMessage = (handlerInput, message) =>
  handlerInput.responseBuilder
    .speak(message)
    .withSimpleCard('Barceloneta', message)
    .getResponse();

module.exports = async handlerInput => {
  const timeSlot = handlerInput.requestEnvelope.request.intent.slots.Time;
  const dateSlot = handlerInput.requestEnvelope.request.intent.slots.Date;
  const time = timeSlot.value
  const date = dateSlot.value
  const targetTime = buildTargetTime({time, date});

  const { message, error } = await resolver(targetTime, { requestedTime: time, requestedDate: date });
  const slots = handlerInput.requestEnvelope.request.intent.slots;

  return message
    ? handleMessage(handlerInput, message)
    : handleError(handlerInput, error);
};
