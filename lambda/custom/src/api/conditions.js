const axios = require('axios');

module.exports = () =>
  axios.get(
    'http://magicseaweed.com/api/' +
      process.env.MSW_KEY +
      '/forecast/?spot_id=3535&units=eu'
  );
