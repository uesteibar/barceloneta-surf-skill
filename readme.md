# Barceloneta Surf - Alexa skill

Pregunta a Alexa si hay olas en la playa de la barceloneta.

Puedes probar a decir:

> Alexa, pregunta a barceloneta surf si hay olas

> Alexa, pregunta a barceloneta surf si se puede surfear mañana

> Alexa, pregunta a barceloneta surf si habrá olas este domingo a las 9
